import Vue from 'vue'
import Router from 'vue-router'
import registerCandidate from '@/components/register-candidate'
import landingPage from '@/components/landingpage'
import detailVacancies from '@/components/detailVacancies'
import loginCandidate from '@/components/login-candidate'
import profilPage from '@/components/profilpage'
import verifEmail from '@/components/verifEmail'
import editProfile from '@/components/editProfile'
import experiencesCandidate from '@/components/experiencesCandidate'
import skillsCandidate from '@/components/skillsCandidate'
import formAplikasi from '@/components/formAplikasi'
import applyList from '@/components/applyList'
import test from '@/components/test'
import verif from '@/components/verif'
import uploadCV from '@/components/dokumen-uploadCV'
import uploadKTP from '@/components/dokumen-uploadKTP'
import uploadKK from '@/components/dokumen-uploadKK'
import uploadNPWP from '@/components/dokumen-uploadNPWP'
import uploadRekening from '@/components/dokumen-uploadRekening'
import uploadBPJSKerja from '@/components/dokumen-uploadBPJSKerja'
import uploadBPJSSehat from '@/components/dokumen-uploadBPJSSehat'
import experienceadd from '@/components/experienceadd'
import skilladd from '@/components/skilladd'
import program from '@/components/program'
import aboutus from '@/components/aboutus'
import forgotpassword from '@/components/forgotpassword'
import resetpassword from '@/components/resetpassword'
import editExperience from '@/components/editExperience'
import editskill from '@/components/editskill'
// import switchlocale from '@/components/switchlocale'

Vue.use(Router)

const router = new Router({
  base: '/job',
  routes: [
    {
      path: '/',
      name: 'landingpage',
      component: landingPage
    },
    {
      path: '/register',
      name: 'register-candidate',
      component: registerCandidate
    },
    {
      path: '/detailvacancies/:id',
      name: 'detailVacancies',
      component: detailVacancies
    },
    {
      path: '/login',
      name: 'login-candidate',
      component: loginCandidate
    },
    {
      path: '/profilpage',
      name: 'profilpage',
      component: profilPage
    },
    {
      path: '/verifemail/:id',
      name: 'VerifEmail',
      component: verifEmail
    },
    {
      path: '/editprofile/:id',
      name: 'editProfile',
      component: editProfile
    },
    {
      path: '/experiencescandidate/:id',
      name: 'experiencesCandidate',
      component: experiencesCandidate
    },
    {
      path: '/skillscandidate/:id',
      name: 'skillsCandidate',
      component: skillsCandidate
    },
    {
      path: '/formaplikasi/:id',
      name: 'formAplikasi',
      component: formAplikasi
    },
    {
      path: '/test',
      name: 'test',
      component: test
    },
    {
      path: '/applylist/:id',
      name: 'applyList',
      component: applyList
    },
    { path: '/verif',
      name: 'Verif',
      component: verif
    },
    {
      path: '/upload_cv/:id',
      name: 'uploadCV',
      component: uploadCV
    },
    {
      path: '/upload_ktp/:id',
      name: 'uploadKTP',
      component: uploadKTP
    },
    {
      path: '/upload_kk/:id',
      name: 'uploadKK',
      component: uploadKK
    },
    {
      path: '/upload_npwp/:id',
      name: 'uploadNPWP',
      component: uploadNPWP
    },
    {
      path: '/upload_rekening/:id',
      name: 'uploadRekening',
      component: uploadRekening
    },
    {
      path: '/upload_bpjs_kerja/:id',
      name: 'uploadBPJSKerja',
      component: uploadBPJSKerja
    },
    {
      path: '/upload_bpjs_sehat/:id',
      name: 'uploadBPJSSehat',
      component: uploadBPJSSehat
    },
    {
      path: '/experienceadd/:id',
      name: 'experienceadd',
      component: experienceadd
    },
    {
      path: '/skilladd/:id',
      name: 'skilladd',
      component: skilladd
    },
    {
      path: '/program',
      name: 'program',
      component: program
    },
    {
      path: '/aboutus',
      name: 'aboutus',
      component: aboutus
    },
    {
      path: '/forgotpassword/',
      name: 'forgotpassword',
      component: forgotpassword
    },
    {
      path: '/resetpassword/:id',
      name: 'resetpassword',
      component: resetpassword
    },
    {
      path: '/editexperience/:id',
      name: 'editExperience',
      component: editExperience
    },
    {
      path: '/editskill/:id',
      name: 'editskill',
      component: editskill
    }

    //  {
    //    path: 'switchlocale',
    //    name: 'switchlocale',
    //    component: switchlocale
    //  }
  ],
  scrollBehavior (to, from, savedPosition) {
    if (to.hash) {
      return { selector: to.hash }
    } else if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

const authenticated = window.sessionStorage.getItem('kandidatLogin')
const roles = window.sessionStorage.getItem('roles')
console.log(roles)
router.beforeEach((to, from, next) => {
  if (to.name !== 'landingpage' && to.name !== 'resetpassword' && to.name !== 'forgotpassword' && to.name !== 'login-candidate' && to.name !== 'register-candidate' && to.name !== 'program' && to.name !== 'aboutus' && to.name !== 'detailVacancies' && to.name !== 'Verif' && to.name !== 'VerifEmail' && authenticated == null) next({name: 'landingpage'})
  else next()
})
export default router

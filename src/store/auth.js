import axios from 'axios'
import Vue from 'vue'
import environtment from '../environtment/index'
import router from '../router/index'
import VueSimpleAlert from 'vue-simple-alert'
import VueSweetalert2 from 'vue-sweetalert2'

Vue.use(VueSimpleAlert)
Vue.use(VueSweetalert2)
Vue.use(axios)
export default {
  state: {
    token: null,
    user: null
  },

  mutations: {

  },

  actions: {
    async signIn (_, credentials) {
      axios.post(environtment.apiUrl + '/api/candidate/logincandidate', {
        email: this.email,
        password: this.password
      }).then(response => {
        console.log(response)
        // eslint-disable-next-line eqeqeq
        if (response.data.data[0].email && response.data.data[0].status == 'active') {
          this.$fire({
            title: 'Congratulation!',
            text: 'Login Success',
            type: 'success'
          }).then(r => {
            router.push({ path: '/' }).then(router.go())
          })
          sessionStorage.setItem('kandidatLogin', JSON.stringify(response.data.data[0]))
          // eslint-disable-next-line eqeqeq
        } else if (response.data.data[0].status == 'notActive') {
          this.$swal({
            icon: 'error',
            title: 'Login Failed!',
            text: 'Please Verify Account!'
          })
          this.$router.push({path: 'verifemail/' + JSON.stringify(response.data.data[0].id)}).then(this.$router.go())
        } else {
          this.$swal({
            icon: 'error',
            title: 'Login Failed!',
            text: 'Email or Password Are Wrong!'
          })
        }
      }
      )
    }
  }
}

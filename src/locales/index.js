import Vue from "vue"
import VueI18n from "vue-i18n"
import English from "../English"
import Indonesia from "../Indonesia"
Vue.use(VueI18n)


const messages = {
  English: English,
  Indonesia: Indonesia,
}

// Create VueI18n instance with options
export default new VueI18n({
  // locale: 'English',
  messages, // set locale messages
  locale: localStorage.getItem('lang'),
  // fallbackLocale: 'en',

})

